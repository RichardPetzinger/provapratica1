using NUnit.Framework;


namespace ProvaPratica.Tests
{
    public class Tests
    {

        [Test]
        public void CPF()
        {
            
            Assert.AreEqual(true, ProvaPratica.Validador.CPF("04294293901"));
        }

        [Test]
        public void CNPJ()
        {
            
            Assert.AreEqual(true, ProvaPratica.Validador.CNPJ("57733936058142"));
        }
    }

}